/*
 * This file provides a TemplateCounter
 * The idea was taken from: https://stackoverflow.com/a/6174263/7224208
 * 
 * The benefit of this counter is it (in theory) has a lower depth for the template resolution and therefor can count larger numbers
 * without increasing the max depth of the compiler.
 * The drawback seems to be a rather long compile time.
 */

#pragma once

#include <type_traits>
#include <cinttypes>

#define __TEMPLH_INTERNAL_NS templatehell::counter::internal

namespace __TEMPLH_INTERNAL_NS
{

/**
 * Internally used intagral constant type for "size_t"
 */
template <std::size_t _Value>
using counter_value = std::integral_constant<std::size_t, _Value>;


} // namespace templatehell::counter::internal


/// INTERNAL! Expands an id (given by the user) to a internal id by prepending a hopefully unused prefix
#define __TEMPLH_INTERNAL_EXPID(id) _templh_counter_##id
/// INTERNAL! Expands to a constant expression that represents the current state of one bit
#define __TEMPLH_INTERNAL_GETBIT(id, val, acc) decltype(__templh_counter_store(id(), __TEMPLH_INTERNAL_NS::counter_value<val>(), __TEMPLH_INTERNAL_NS::counter_value<acc>()))::value
/// INTERNAL! Expands to a constant expression that represents the current state of one byte
#define __TEMPLH_INTERNAL_GETBYTE(id, start, acc) __TEMPLH_INTERNAL_GETBIT(id, start,                                                                                                          \
                                                                          __TEMPLH_INTERNAL_GETBIT(id, start * 2,                                                                             \
                                                                                                   __TEMPLH_INTERNAL_GETBIT(id, start * 4,                                                    \
                                                                                                                            __TEMPLH_INTERNAL_GETBIT(id, start * 8,                           \
                                                                                                                                                     __TEMPLH_INTERNAL_GETBIT(id, start * 16, \
                                                                                                                                                                              __TEMPLH_INTERNAL_GETBIT(id, start * 32, acc))))))

/**
 * This function is declared but no body defined since we only care about the return value.
 * It will get overloaded by every new count for every counter but never called.
 * The state is only represented by the specialized parameter and return types.
 * In an ideal case this would work inside a namespace, but sadly it doesnt :(.
 * INTERNAL USE ONLY
 */
template <typename _Id, std::size_t _Value, std::size_t _Accumulator>
__TEMPLH_INTERNAL_NS::counter_value<_Accumulator> __templh_counter_store(_Id, __TEMPLH_INTERNAL_NS::counter_value<_Value>, __TEMPLH_INTERNAL_NS::counter_value<_Accumulator>);

/// Expands to a new type declaration (in this case a struct) for the counter, the type is only used for specialization
#define TEMPLH_COUNTER_CREATE(id) \
    struct _templh_counter_##id   \
    {                             \
    }
/// Gets the current value of the counter 
#define TEMPLH_COUNTER_GET(id) __TEMPLH_INTERNAL_GETBYTE(__TEMPLH_INTERNAL_EXPID(id), 1,0)
/// Increments the counter
#define TEMPLH_COUNTER_INC(id)                                                                      \
    __TEMPLH_INTERNAL_NS::counter_value<TEMPLH_COUNTER_GET(id) + 1> __templh_counter_store(                   \
        _templh_counter_##id,                                                                        \
        __TEMPLH_INTERNAL_NS::counter_value<(TEMPLH_COUNTER_GET(id) + 1) & ~TEMPLH_COUNTER_GET(id)>, \
        __TEMPLH_INTERNAL_NS::counter_value<(TEMPLH_COUNTER_GET(id) + 1) & TEMPLH_COUNTER_GET(id)>);


/* Theory of operation:
 * The counter works by declaring/overloading the state function (__templh_counter_store) 
 * to return the next counter state when the bit value is equal to the lowest bit that is set to 1 after incrementing the previous state
 * and the accumulator equals the counter state returned by the upper bits.
 * 
 * c+1 & ~c => Returns the lowest bit set after the increment since a increment should always only set 1 bit all bits below that newly set 1 will be 0
 * (basically a overflow to the next digit). So "c+1" will have that "new" bit set and the ones below 0 while "c" has all below set to 1 and
 * the newly set bit is 0 (before the increment). This is inverted so the only place where two 1's can be on both sides of the AND is at the position
 * where the increment will set a bit to 1.
 * 
 * c+1 & c => Returns the value of all the bits above the bit that gets set to 1 by the increment. "c+1" will have that "newly" set bit set to 1 and the ones
 * below 0 while "c" will have the "new" bit set to 0 and all the ones below 1. The only bits where both "c+1" and "c" are the same is above that newly set bit.
 * 
 * |current   | new    | c+1 & ~c        | c+1 & c        |
 * |0         | 1      | 0001 = 1        | 0000 = 0       | newly set bit is bit #0 and the bits above equal 0
 * |1         | 2      | 0010 = 2        | 0000 = 0       | newly set bit is bit #1 and the bits above equal 0
 * |2         | 3      | 0001 = 1        | 0010 = 2       | newly set bit is bit #0 and the bits above equal 2
 * |3         | 4      | 0100 = 4        | 0000 = 0       | newly set bit is bit #2 and the bits above equal 0
 * |4         | 5      | 0001 = 1        | 0100 = 4       | newly set bit is bit #0 and the bits above equal 4
 * |5         | 6      | 0010 = 2        | 0100 = 4       | newly set bit is bit #1 and the bits above equal 4
 * |6         | 7      | 0001 = 1        | 0100 = 6       | newly set bit is bit #0 and the bits above equal 6
 * |7         | 8      | 1000 = 8        | 0000 = 0       | newly set bit is bit #3 and the bits above equal 0
 * |8         | 9      | 0001 = 1        | 0100 = 8       | newly set bit is bit #0 and the bits above equal 8
 */