#pragma once

template<typename T, T ..._Other>
struct TemplateList;

template<typename T>
struct TemplateList<T>{
    static constexpr T array[] = {};
};

template <typename T, T _This, T ..._Other>
struct TemplateList<T, _This, _Other...> : TemplateList<T, _Other...>
{ 
    static constexpr T value = _This;
    static constexpr T array[] = {_This, _Other...};
};
