#include <iostream>
#include "TemplateCounter.hpp"

TEMPLH_COUNTER_CREATE(test);

TEMPLH_COUNTER_INC(test);
constexpr size_t prev = TEMPLH_COUNTER_GET(test);
TEMPLH_COUNTER_INC(test);
TEMPLH_COUNTER_INC(test);
TEMPLH_COUNTER_INC(test);
TEMPLH_COUNTER_INC(test);
constexpr size_t next = TEMPLH_COUNTER_GET(test);

int main(int argc, char **argv)
{
    std::cout << prev << std::endl << next << std::endl;
    return 0;
}