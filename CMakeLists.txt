
cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0048 NEW)
project(TemplateHell LANGUAGES CXX VERSION 0.0.1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(${CMAKE_SOURCE_DIR}/src)